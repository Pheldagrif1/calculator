from input import get_full_input
from calculate import calcuate

from os import system

from art import title

print(title)
first_num, second_num, op = get_full_input()
answer = calcuate(first_num,second_num, op)

if answer != None:
    answer = str(answer).rstrip('0').rstrip('.')
    print(f"Your answer is: {answer} ")

keep_going = input("Do you want to do another calculation? Type 'Y' for yes, anything else for No: ")

while keep_going == 'Y' or keep_going == 'y':
    system('cls')
    print(title)
    first_num, second_num, op = get_full_input()
    answer = calcuate(first_num,second_num, op)

    if answer != None:
        print(f"Your answer is: {answer} ")

    keep_going = input("Do you want to do another calculation? Type 'Y' for yes, anything else for No: ")
    print(keep_going)
print("It has been fun, bye!")

    
    


