from validate import validate_number

def get_full_input():
    valid_operators = ["+","-","*","/","**","sq","%"]
    number1 = input("Enter your first number: ")
    while validate_number(number1) == False:
         number1 = input("Please enter a decimal number: ")
 
    number2 = input("Enter your second number: ")
    while validate_number(number2) == False:
         number2 = input("Please enter a decimal number: ")    
    
    
    print("Your operator choices are: \n \
          +: add\n\
          -: subtract\n\
          *: multiply\n\
          /: divide\n\
          **: exponent\n\
          %: get remainder\n")
            
    operator = ""
    while operator not in valid_operators:
        if operator == "":
            operator = input("Enter your operator: ")
        else: 
            operator = input("Invalid operator!  Please choose from the list above: ")
    return number1, number2, operator