def validate_number(number):
    try:
        float(number)
        return True
    except ValueError:
        return False