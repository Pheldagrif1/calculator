def calcuate(num_1, num_2, operator): 
    num1 = float(num_1)
    num2 = float(num_2)

    if operator == "+":
        return num1 + num2
    elif operator == "-":
        return num1 - num2
    elif operator == "*":
        return num1 * num2
    elif operator == "/":
        if num2 == 0:
            print("You can't divide by 0!  Sorry!")
            return None
        else:
            return num1 / num2
    elif operator == "**":
        return num1 ** num2    
    elif operator == "%":
        return num1 % num2 